var elem = document.documentElement;
function goFullscreen() {
  if (elem.requestFullscreen) {
    elem.requestFullscreen();
  } else if (elem.mozRequestFullScreen) { /* Firefox */
    elem.mozRequestFullScreen();
  } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
    elem.webkitRequestFullscreen();
  } else if (elem.msRequestFullscreen) { /* IE/Edge */
    elem.msRequestFullscreen();
  }
}

function closeFullscreen() {
  if (document.exitFullscreen) {
    document.exitFullscreen();
  } else if (document.mozCancelFullScreen) {
    document.mozCancelFullScreen();
  } else if (document.webkitExitFullscreen) {
    document.webkitExitFullscreen();
  } else if (document.msExitFullscreen) {
    document.msExitFullscreen();
  }
}

if (document.addEventListener)
{
 document.addEventListener('fullscreenchange', exitHandler, false);
 document.addEventListener('mozfullscreenchange', exitHandler, false);
 document.addEventListener('MSFullscreenChange', exitHandler, false);
 document.addEventListener('webkitfullscreenchange', exitHandler, false);
}

function exitHandler(a, e)
{
 if (document.webkitIsFullScreen || document.mozFullScreen || document.msFullscreenElement !== null)
 {
  if (document.getElementById('goFullscreen').style.display == "none"){
    document.getElementById('closeFullscreen').style.display="none";
    document.getElementById('goFullscreen').style.display="block";
  }
  else
    {
    document.getElementById('goFullscreen').style.display="none";
    document.getElementById('closeFullscreen').style.display="block";
  }
 }
}
